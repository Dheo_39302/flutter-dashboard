import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dashboard Home',

      home: Dashboard('Dashboard Home'),
    );
  }

  Widget Dashboard(String Tittle){
    
    final Dashboard = Container(
      margin: EdgeInsets.fromLTRB(0.5, 0, 20, 0),
    child: Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
      "Home",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 22.0,
            ),
          ),
      Text(
        "Dashboard Home",
        style: TextStyle(
          fontWeight: FontWeight.w300,
          fontSize: 15.0,
              ),
            ),
          ],
        ),
      ),
    );


    final Admin = Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          Container(
          margin: EdgeInsets.all(5),
            child: Icon(Icons.home, size: 18,)
        ),
        Text(
        "Admin Page  ",
        style: TextStyle(
          fontWeight: FontWeight.w200,
          fontSize: 15.0,
            ),
          ),
          Icon(Icons.arrow_forward_ios_rounded, size: 13,),
          Text(
            "   Dashboard",
              style: TextStyle(
              fontWeight: FontWeight.w800,
              fontSize: 15.0,
                color: Colors.blue,
            ),
          ),
        ],
      ),
    );

    final text = Expanded(child: Container(
      width: 400,
      height: 100,
      padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
      child: Card(
        elevation: 5,
        child: Stack(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.all(5),
                    child: Icon(
                      Icons.info_outline_rounded,
                      size: 70,
                    ),
                  ),
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "20 M",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22.0,
                              ),
                            ),
                          ),
                          Text(
                            "Total Omzet Total Omzet Total Omzet Total Omzet ",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 15.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: 5,
              right: 5,
              child: Container(
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(Icons.keyboard_arrow_up_rounded,
                        size: 12.0,
                        color: Colors.green),
                    Text("20.23%",
                        style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.green)
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return Scaffold(
    appBar: AppBar(
      title: Text('Dashboard Home'),
      ),
    body: ListView(
          children: [
            SizedBox(height: 20.0,),
            Padding(padding: EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Dashboard,
                Admin,
              ],
              ),
            ),
            Container(
              child: Row(
                children: [
                  text,
                  text,
                  text,
                  text,
                ],
              ),
            ),
          ],
        ),
    );
  }

}



